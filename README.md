# MergeLogs
This application merges all files in a directory to stdout. Assuming each file
consists of lexicographically ordered lines, the output is lexicographically
ordered. 

## Usage example
```shell script
cargo run example/
```
## Assumptions
These conditions will exit with a non-zero return code, and a clear explanation:

1. Directory contains subdirectories or symlinks.

2. Any read errors.

These conditions may cause unexpected behavior:

1. Incorrectly formatted lines may be printed in the wrong order.

2. Very long lines will result in high memory usage.
