use std::cmp::Ordering;

pub struct QueuedLog<R> {
    pub name: String,
    pub current_line: String,
    pub line_number: u64,
    pub remaining: R,
}

impl<R> PartialEq for QueuedLog<R> {
    fn eq(&self, other: &QueuedLog<R>) -> bool {
        self.cmp(other) == Ordering::Equal
    }
}
impl<R> Eq for QueuedLog<R> {}
impl<R> PartialOrd for QueuedLog<R> {
    fn partial_cmp(&self, other: &QueuedLog<R>) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl<R> Ord for QueuedLog<R> {
    fn cmp(&self, other: &QueuedLog<R>) -> Ordering {
        self.current_line.cmp(&other.current_line)
            .then_with(||self.name.cmp(&other.name))
            .then_with(||self.line_number.cmp(&other.line_number))
    }
}
