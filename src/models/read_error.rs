use std::{
    error::Error,
    fmt::{self, Display, Formatter},
    io,
};

#[derive(Debug)]
pub struct ReadError {
    pub file_name: String,
    pub line_number: u64,
    pub error: io::Error,
}

impl Display for ReadError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Failed to read from the log named {} at line {}",
            self.file_name, self.line_number
        )
    }
}

impl Error for ReadError {}
