pub use std::{
    ffi::OsString,
    fmt::{Debug, Display, Formatter, Result},
    io,
};

#[derive(Debug)]
pub enum InitializeError {
    CouldntListDirectory(String, io::Error),
    NotSimpleFile(String),
    FailedOpeningFile(String, io::Error),
    BadFileName(OsString),
}

impl Display for InitializeError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        let msg = match self {
            Self::CouldntListDirectory(d, e) => {
                format!("Failed to list the directory {}, {}", d, e)
            }
            Self::NotSimpleFile(f) => format!(
                "The entry at {} is not a plain file. Directories \
              containing symlinks or subdirectories are not supported.",
                f
            ),
            Self::FailedOpeningFile(file_name, e) => {
                format!("Failed to open the file [{}]: {}", file_name, e)
            }
            Self::BadFileName(_) => String::from("Unable to handle a file's name."),
        };
        write!(f, "Failed to initialize: {}", msg)
    }
}
