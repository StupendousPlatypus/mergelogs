use std::{
    env::{args as get_args, Args},
    fs::{self, File},
    io::{stdout, BufReader},
    process::exit,
};

mod merge_readers;
mod models;

use merge_readers::merge_readers;
use models::InitializeError;

enum AppError {
    BadArgs { correct_usage: String },
    InitializeError(models::InitializeError),
    ReadErrors(Vec<models::ReadError>),
}

fn main() {
    wrapped_main().unwrap_or_else(|e| {
        match e {
            AppError::BadArgs { correct_usage } => {
                eprintln!("Bad arguments. Correct usage: {}", correct_usage)
            }
            AppError::InitializeError(e) => eprintln!("{}", e),
            AppError::ReadErrors(es) => {
                eprintln!("Not all files were read to completion:");
                for e in es {
                    eprintln!("{}", e)
                }
            }
        }
        exit(1)
    })
}

fn wrapped_main() -> Result<(), AppError> {
    let source_dir = directory_from_args(get_args())?;

    let output = stdout();
    let mut output_handle = output.lock();

    let files = open_dir(&source_dir).map_err(AppError::InitializeError)?;

    let read_errors = merge_readers(files, &mut output_handle);

    if read_errors.is_empty() {
        Ok(())
    } else {
        Err(AppError::ReadErrors(read_errors))
    }
}

fn directory_from_args(args: Args) -> Result<String, AppError> {
    match args.collect::<Vec<String>>().as_slice() {
        [_, source_dir] => Ok(source_dir.to_string()),
        [first, ..] => Err(AppError::BadArgs {
            correct_usage: format!("{} source_directory/", first),
        }),
        _ => unreachable!(),
    }
}

fn open_dir(source_dir: &str) -> Result<Vec<(String, BufReader<File>)>, InitializeError> {
    use InitializeError::*;

    let entries =
        fs::read_dir(source_dir).map_err(|e| CouldntListDirectory(String::from(source_dir), e))?;

    let mut vec = Vec::new();

    for entry_try in entries {
        let entry = entry_try.map_err(|e| CouldntListDirectory(String::from(source_dir), e))?;
        let entry_name = entry
            .path()
            .into_os_string()
            .into_string()
            .map_err(BadFileName)?;
        let file_type = entry
            .file_type()
            .map_err(|e| FailedOpeningFile(entry_name.clone(), e))?;
        if !file_type.is_file() {
            return Err(NotSimpleFile(entry_name));
        }

        let file =
            File::open(entry.path()).map_err(|e| FailedOpeningFile(entry_name.clone(), e))?;
        let buf_reader = BufReader::new(file);
        vec.push((entry_name, buf_reader))
    }

    Ok(vec)
}
