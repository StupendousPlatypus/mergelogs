use std::{
    cmp::Reverse,
    collections::BinaryHeap,
    io::{BufRead, BufReader, Read, Write},
};

use crate::models::*;

type MergeQueue<R> = BinaryHeap<Reverse<QueuedLog<BufReader<R>>>>;

pub fn merge_readers<R: Read, W: Write>(logs: Vec<(String, R)>, output: &mut W) -> Vec<ReadError> {
    let (heap, mut read_errors) = init_queue::<R>(logs);
    consume_queue(output, heap, &mut read_errors);
    read_errors
}

fn consume_queue<R: Read, W: Write>(
    output: &mut W,
    mut queue: MergeQueue<R>,
    errors: &mut Vec<ReadError>,
) {
    while let Some(Reverse(log)) = queue.pop() {
        let try_next = advance_log(output, log);
        match try_next {
            Ok(Some(next)) => queue.push(Reverse(next)),
            Ok(None) => (),
            Err(err) => errors.push(err),
        }
    }
}

fn advance_log<R, W>(
    output: &mut W,
    previous_log: QueuedLog<R>,
) -> Result<Option<QueuedLog<R>>, ReadError>
where
    R: BufRead,
    W: Write,
{
    let QueuedLog {
        name,
        current_line,
        line_number,
        mut remaining,
    } = previous_log;

    output
        .write_all(current_line.as_bytes())
        .unwrap_or_else(|e| panic!(e));

    let mut line = String::new();
    let read = remaining.read_line(&mut line).map_err(|e| ReadError {
        file_name: name.clone(),
        line_number: line_number + 1,
        error: e,
    })?;

    if read == 0 {
        let len = current_line.len();
        if len > 0 {
            let last_char = current_line.as_bytes()[len - 1];
            if last_char != b'\n' {
                output.write_all(b"\n").unwrap_or_else(|e| panic!(e));
            }
        }
        Ok(None)
    } else {
        Ok(Some(QueuedLog {
            name,
            current_line: line,
            line_number: line_number + 1,
            remaining,
        }))
    }
}

fn init_queue<R: Read>(logs: Vec<(String, R)>) -> (MergeQueue<R>, Vec<ReadError>) {
    let mut heap = BinaryHeap::new();
    let mut errors = Vec::new();

    for (name, reader) in logs {
        let mut line = String::new();
        let mut buf_reader = BufReader::new(reader);
        let read_result = buf_reader.read_line(&mut line);
        match read_result {
            Ok(_) => {
                let log = QueuedLog {
                    name,
                    current_line: line,
                    line_number: 0,
                    remaining: buf_reader,
                };
                heap.push(Reverse(log));
            }
            Err(e) => errors.push(ReadError {
                file_name: name.clone(),
                line_number: 0,
                error: e,
            }),
        }
    }

    (heap, errors)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{io::BufReader, str};

    const SERVER_A_LOG: (&str, &str) = (
        "ServerA",
        "2016-12-20T19:00:45Z, Server A started.\n\
            2016-12-20T19:01:25Z, Server A completed job.\n\
            2016-12-20T19:02:48Z, Server A terminated.",
    );

    const SERVER_B_LOG: (&str, &str) = (
        "ServerB",
        "2016-12-20T19:01:16Z, Server B started.\n\
            2016-12-20T19:03:25Z, Server B completed job.\n\
            2016-12-20T19:04:50Z, Server B terminated.\n",
    );

    const EMPTY_LOG: (&str, &str) = ("Empty", "");

    fn merge_examples<'a>(samples: &[(&str, &'a str)]) -> (Vec<u8>, Vec<ReadError>) {
        let readers = samples
            .iter()
            .map(|(name, data)| (String::from(*name), BufReader::new(data.as_bytes())))
            .collect();
        let mut write_vec = Vec::new();
        let errors = merge_readers(readers, &mut write_vec);

        (write_vec, errors)
    }

    #[test]
    fn returns_exact_for_single_log() {
        let (output, errors) = merge_examples(&[SERVER_B_LOG]);

        assert!(errors.is_empty());

        assert_eq!(SERVER_B_LOG.1, str::from_utf8(&output).unwrap());
    }

    #[test]
    fn merges_a_and_b() {
        let expected = "2016-12-20T19:00:45Z, Server A started.\n\
            2016-12-20T19:01:16Z, Server B started.\n\
            2016-12-20T19:01:25Z, Server A completed job.\n\
            2016-12-20T19:02:48Z, Server A terminated.\n\
            2016-12-20T19:03:25Z, Server B completed job.\n\
            2016-12-20T19:04:50Z, Server B terminated.\n\
    ";

        let (output, errors) = merge_examples(&[SERVER_A_LOG, SERVER_B_LOG, EMPTY_LOG]);
        assert!(errors.is_empty());
        assert_eq!(expected, str::from_utf8(&output).unwrap());
    }
}
